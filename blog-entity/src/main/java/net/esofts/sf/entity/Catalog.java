package net.esofts.sf.entity;

import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotation.TableName;
import net.esofts.sf.base.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author dachong
 * @since 2019-12-08
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("BS_CATALOG")
public class Catalog extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 分类名称
     */
    @TableField("NAME")
    private String name;

    /**
     * 状态 0禁用 1正常
     */
    @TableField("STATE")
    private Integer state;

    /**
     * 父节点ID
     */
    @TableField("PARENT")
    private BigDecimal parent;

    /**
     * 顺序
     */
    @TableField("ORDER")
    private Integer order;


}
