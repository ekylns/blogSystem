package net.esofts.sf.entity;

import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotation.TableName;
import net.esofts.sf.base.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author dachong
 * @since 2019-12-08
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("BS_USER")
public class User extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 登录名
     */
    @TableField("DLM")
    private String dlm;

    /**
     * 登录密码
     */
    @TableField("DLP")
    private String dlp;

    /**
     * 头像base64编码
     */
    @TableField("PHOTO")
    private String photo;

    /**
     * 昵称
     */
    @TableField("NICK")
    private String nick;

    /**
     * 状态
     */
    @TableField("STATE")
    private BigDecimal state;


}
