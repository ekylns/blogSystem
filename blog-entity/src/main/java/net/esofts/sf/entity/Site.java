package net.esofts.sf.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import net.esofts.sf.base.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author dachong
 * @since 2019-12-08
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("BS_SITE")
public class Site extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 站点名称
     */
    @TableField("NAME")
    private String name;

    /**
     * 邮箱
     */
    @TableField("EMAIL")
    private String email;

    /**
     * 工信ICP
     */
    @TableField("ICP")
    private String icp;

    /**
     * 公安ICP
     */
    @TableField("PICP")
    private String picp;

    /**
     * 电话
     */
    @TableField("TEL")
    private String tel;


}
