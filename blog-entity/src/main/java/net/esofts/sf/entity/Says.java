package net.esofts.sf.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import net.esofts.sf.base.BaseEntity;

/**
 * <p>
 * 
 * </p>
 *
 * @author dachong
 * @since 2019-12-11
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("bs_says")
public class Says extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @TableField("MSG")
    private String msg;


}
