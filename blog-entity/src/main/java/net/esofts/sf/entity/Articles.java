package net.esofts.sf.entity;

import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotation.TableName;
import net.esofts.sf.base.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author dachong
 * @since 2019-12-08
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("BS_Articles")
public class Articles extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 文章标题
     */
    @TableField("TITLE")
    private String title;

    /**
     * 文章关键字
     */
    @TableField("KEYWORDS")
    private String keywords;

    /**
     * 描述
     */
    @TableField("DESCRIPTION")
    private String description;

    /**
     * 分类ID
     */
    @TableField("CATALOG")
    private BigDecimal catalog;

    /**
     * 置顶 0 否 1是
     */
    @TableField("IS_TOP")
    private Integer isTop;

    /**
     * 是否推荐 0否 1是
     */
    @TableField("IS_REC")
    private Integer isRec;

    /**
     * 状态0 保存 1发布
     */
    @TableField("STATE")
    private Integer state;

    /**
     * 外链地址
     */
    @TableField("VIEW_ID")
    private String viewId;

    /**
     * 自定义地址
     */
    @TableField("VIEW_URL")
    private String viewUrl;

    /**
     * 阅读数量
     */
    @TableField("VIEWS")
    private BigDecimal views;

    /**
     * 标签
     */
    @TableField("TAGS")
    private String tags;

    /**
     * 厌恶数量
     */
    @TableField("HATES")
    private BigDecimal hates;

    /**
     * 喜爱、有用数量
     */
    @TableField("LOVES")
    private BigDecimal loves;

    /**
     * 图片地址
     */
    @TableField("PIC")
    private String pic;


}
