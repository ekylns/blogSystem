package net.esofts.sf.entity;

import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotation.TableName;
import net.esofts.sf.base.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author dachong
 * @since 2019-12-08
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("BS_Content")
public class Content extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @TableField("ARTICLE")
    private BigDecimal article;

    @TableField("CONTENT")
    private String content;


}
