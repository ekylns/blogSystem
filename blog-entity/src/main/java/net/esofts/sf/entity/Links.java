package net.esofts.sf.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import net.esofts.sf.base.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author dachong
 * @since 2019-12-08
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("BS_Links")
public class Links extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 链接标题
     */
    @TableField("TITLE")
    private String title;

    /**
     * 链接地址
     */
    @TableField("LINK")
    private String link;

    /**
     * 状态 0 禁用 1可用
     */
    @TableField("STATE")
    private Integer state;

    /**
     * 顺序
     */
    @TableField("ORDER")
    private Integer order;


}
