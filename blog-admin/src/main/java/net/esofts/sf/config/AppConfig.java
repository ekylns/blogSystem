package net.esofts.sf.config;

import net.esofts.sf.entity.Site;
import net.esofts.sf.service.ISiteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.servlet.ServletContext;

@Component
public class AppConfig {
    @Resource
    private ServletContext servletContext;
    @Resource
    private ISiteService siteService;
    @PostConstruct
    public void init() {
        Site site=siteService.getById(1);

        System.out.println("初始化" +servletContext.getServerInfo()+"=");
    }
}
