package net.esofts.sf.controller;

import cn.hutool.captcha.CaptchaUtil;
import cn.hutool.captcha.ICaptcha;
import net.esofts.sf.entity.Says;
import net.esofts.sf.entity.Site;
import net.esofts.sf.service.ISiteService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 登录首页控制器
 */
@Controller
public class HomeController {

    @Resource
    private ISiteService siteService;

    //后台首页
    @GetMapping("/")
    public String index() {
       Site site=siteService.getById(1);
        Says says=new Says();
        says.setMsg("心情不好");
        return "index";
    }

    //验证码
    @GetMapping("captcha")
    public void captcha(HttpServletRequest req, HttpServletResponse res) throws IOException {
        ICaptcha captcha = CaptchaUtil.createShearCaptcha(200, 100, 4, 4);
        req.getSession().setAttribute("captcha",captcha.getCode());
        captcha.write(res.getOutputStream());
        res.getOutputStream().close();
    }

    //登录请求
    @PostMapping("/login")
    public String login() {
        return "main";
    }
}
