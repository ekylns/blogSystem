package net.esofts.sf;


import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.InjectionConfig;
import com.baomidou.mybatisplus.generator.config.DataSourceConfig;
import com.baomidou.mybatisplus.generator.config.GlobalConfig;
import com.baomidou.mybatisplus.generator.config.PackageConfig;
import com.baomidou.mybatisplus.generator.config.StrategyConfig;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;
import com.baomidou.mybatisplus.generator.engine.FreemarkerTemplateEngine;

import java.util.HashMap;
import java.util.Map;

//代码生成工具
public class GeneratorCode {
    private static String jdbcUrl = "jdbc:mysql://127.0.0.1:3306/blog-system?useUnicode=true&characterEncoding=utf-8&serverTimezone=UTC";
    private static String user = "root";
    private static String passwd = "123456";
    private static String driverClass = "com.mysql.jdbc.Driver";

    public static void main(String[] args) {
        // 代码生成器
        AutoGenerator mpg = new AutoGenerator();
        // 全局配置
        GlobalConfig gc = new GlobalConfig();
        String projectPath = "blog-generator";
        gc.setOutputDir(projectPath + "/src/main/java");
        gc.setAuthor("dachong");
        gc.setOpen(true);
        // gc.setSwagger2(true); 实体属性 Swagger2 注解

        mpg.setGlobalConfig(gc);
        // 数据源配置
        DataSourceConfig dsc = new DataSourceConfig();
        dsc.setUrl(jdbcUrl);
        dsc.setDriverName(driverClass);
        dsc.setUsername(user);
        dsc.setPassword(passwd);
        mpg.setDataSource(dsc);
        // 包配置
        PackageConfig pc = new PackageConfig();
        pc.setParent("net.esofts.sf");
        mpg.setPackageInfo(pc);
        // 策略配置
        StrategyConfig strategy = new StrategyConfig();
        strategy.setNaming(NamingStrategy.underline_to_camel);
        strategy.setColumnNaming(NamingStrategy.underline_to_camel);
        strategy.setSuperEntityClass("net.esofts.sf.base.BaseEntity");
        strategy.setEntityLombokModel(true);
        strategy.setEntityTableFieldAnnotationEnable(true);
        //strategy.setRestControllerStyle(true);
        // 公共父类
        //strategy.setSuperControllerClass("com.baomidou.ant.common.BaseController");
        // 写于父类中的公共字段'
        strategy.setSuperEntityColumns(new String[]{"ID", "CREATER", "CREATE_TIME", "UPDATE_TIME"});
        //strategy.setControllerMappingHyphenStyle(true);
        strategy.setTablePrefix("BS_");
        strategy.setEntityTableFieldAnnotationEnable(true);
        mpg.setStrategy(strategy);

        mpg.setTemplateEngine(new FreemarkerTemplateEngine());
        mpg.execute();

    }
}
