package net.esofts.sf.config;

import net.esofts.sf.entity.Site;
import net.esofts.sf.service.ISiteService;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.servlet.ServletContext;

@Component
public class BlogWebConfig {
    @Resource
    private ServletContext servletContext;
    @Resource
    private ISiteService siteService;

    @PostConstruct
    public void initSiteInfo(){
      Site site=siteService.getById(1);
        servletContext.setAttribute("siteName",site.getName());
    }
}
