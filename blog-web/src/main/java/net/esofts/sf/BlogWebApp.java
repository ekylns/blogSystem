package net.esofts.sf;

import net.esofts.sf.utils.ConstStr;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication
@MapperScan(ConstStr.MAPPER_DIR)
public class BlogWebApp extends SpringBootServletInitializer {
    public static void main(String[] args) {
        SpringApplication.run(BlogWebApp.class);
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        return builder.sources(BlogWebApp.class);
    }
}
