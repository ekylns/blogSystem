package net.esofts.sf.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import net.esofts.sf.entity.Says;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author dachong
 * @since 2019-12-11
 */
public interface SaysMapper extends BaseMapper<Says> {

}
