package net.esofts.sf.mapper;

import net.esofts.sf.entity.Content;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author dachong
 * @since 2019-12-08
 */
public interface ContentMapper extends BaseMapper<Content> {

}
