package net.esofts.sf.service.impl;

import net.esofts.sf.entity.Catalog;
import net.esofts.sf.mapper.CatalogMapper;
import net.esofts.sf.service.ICatalogService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author dachong
 * @since 2019-12-08
 */
@Service
public class CatalogServiceImpl extends ServiceImpl<CatalogMapper, Catalog> implements ICatalogService {

}
