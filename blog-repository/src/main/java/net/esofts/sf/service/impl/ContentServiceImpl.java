package net.esofts.sf.service.impl;

import net.esofts.sf.entity.Content;
import net.esofts.sf.mapper.ContentMapper;
import net.esofts.sf.service.IContentService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author dachong
 * @since 2019-12-08
 */
@Service
public class ContentServiceImpl extends ServiceImpl<ContentMapper, Content> implements IContentService {

}
