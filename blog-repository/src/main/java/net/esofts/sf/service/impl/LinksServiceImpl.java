package net.esofts.sf.service.impl;

import net.esofts.sf.entity.Links;
import net.esofts.sf.mapper.LinksMapper;
import net.esofts.sf.service.ILinksService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author dachong
 * @since 2019-12-08
 */
@Service
public class LinksServiceImpl extends ServiceImpl<LinksMapper, Links> implements ILinksService {

}
