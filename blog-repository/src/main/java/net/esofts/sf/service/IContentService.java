package net.esofts.sf.service;

import net.esofts.sf.entity.Content;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author dachong
 * @since 2019-12-08
 */
public interface IContentService extends IService<Content> {

}
