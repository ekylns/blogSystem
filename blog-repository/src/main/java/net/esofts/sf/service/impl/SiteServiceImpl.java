package net.esofts.sf.service.impl;

import net.esofts.sf.entity.Site;
import net.esofts.sf.mapper.SiteMapper;
import net.esofts.sf.service.ISiteService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author dachong
 * @since 2019-12-08
 */
@Service
public class SiteServiceImpl extends ServiceImpl<SiteMapper, Site> implements ISiteService {

}
