package net.esofts.sf.service.impl;

import net.esofts.sf.entity.User;
import net.esofts.sf.mapper.UserMapper;
import net.esofts.sf.service.IUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author dachong
 * @since 2019-12-08
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements IUserService {

}
