package net.esofts.sf.service;

import net.esofts.sf.entity.Catalog;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author dachong
 * @since 2019-12-08
 */
public interface ICatalogService extends IService<Catalog> {

}
