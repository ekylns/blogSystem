SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for bs_articles
-- ----------------------------
DROP TABLE IF EXISTS `bs_articles`;
CREATE TABLE `bs_articles` (
  `ID` decimal(20,0) NOT NULL,
  `TITLE` varchar(500) NOT NULL COMMENT '文章标题',
  `KEYWORDS` varchar(1000) DEFAULT NULL COMMENT '文章关键字',
  `DESCRIPTION` varchar(1000) DEFAULT NULL COMMENT '描述',
  `CATALOG` decimal(20,0) NOT NULL COMMENT '分类ID',
  `IS_TOP` int(1) DEFAULT '0' COMMENT '置顶 0 否 1是',
  `IS_REC` int(1) DEFAULT '0' COMMENT '是否推荐 0否 1是',
  `STATE` int(1) DEFAULT '0' COMMENT '状态0 保存 1发布',
  `VIEW_ID` varchar(32) DEFAULT NULL COMMENT '外链地址',
  `VIEW_URL` varchar(500) DEFAULT NULL COMMENT '自定义地址',
  `VIEWS` decimal(20,0) DEFAULT '0' COMMENT '阅读数量',
  `TAGS` varchar(500) DEFAULT NULL COMMENT '标签',
  `HATES` decimal(20,0) DEFAULT NULL COMMENT '厌恶数量',
  `LOVES` decimal(20,0) DEFAULT NULL COMMENT '喜爱、有用数量',
  `PIC` varchar(500) DEFAULT NULL COMMENT '图片地址',
  `CREATER` decimal(20,0) NOT NULL,
  `CREATE_TIME` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `UPDATE_TIME` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`ID`),
  KEY `CATALOG_FK` (`CATALOG`),
  CONSTRAINT `CATALOG_FK` FOREIGN KEY (`CATALOG`) REFERENCES `bs_catalog` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of bs_articles
-- ----------------------------

-- ----------------------------
-- Table structure for bs_catalog
-- ----------------------------
DROP TABLE IF EXISTS `bs_catalog`;
CREATE TABLE `bs_catalog` (
  `ID` decimal(20,0) NOT NULL,
  `NAME` varchar(255) NOT NULL COMMENT '分类名称',
  `STATE` int(1) NOT NULL DEFAULT '0' COMMENT '状态 0禁用 1正常',
  `PARENT` decimal(20,0) DEFAULT '-1' COMMENT '父节点ID',
  `ORDER` int(11) DEFAULT NULL COMMENT '顺序',
  `CREATER` decimal(20,0) NOT NULL COMMENT '创建人',
  `CREATE_TIME` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `UPDATE_TIME` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of bs_catalog
-- ----------------------------
INSERT INTO `bs_catalog` VALUES ('1', 'Java技术', '1', '-1', '0', '1', '2019-12-08 21:54:53', null);
INSERT INTO `bs_catalog` VALUES ('2', 'Java基础', '1', '1', '1', '1', '2019-12-08 22:25:12', null);

-- ----------------------------
-- Table structure for bs_content
-- ----------------------------
DROP TABLE IF EXISTS `bs_content`;
CREATE TABLE `bs_content` (
  `ID` decimal(20,0) NOT NULL,
  `ARTICLE` decimal(20,0) NOT NULL,
  `CONTENT` longtext,
  PRIMARY KEY (`ID`),
  KEY `CID` (`ARTICLE`),
  CONSTRAINT `CID` FOREIGN KEY (`ARTICLE`) REFERENCES `bs_articles` (`ID`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of bs_content
-- ----------------------------

-- ----------------------------
-- Table structure for bs_links
-- ----------------------------
DROP TABLE IF EXISTS `bs_links`;
CREATE TABLE `bs_links` (
  `ID` decimal(20,0) NOT NULL,
  `TITLE` varchar(255) NOT NULL COMMENT '链接标题',
  `LINK` varchar(500) NOT NULL COMMENT '链接地址',
  `STATE` int(1) DEFAULT '0' COMMENT '状态 0 禁用 1可用',
  `ORDER` int(11) DEFAULT '0' COMMENT '顺序',
  `CREATER` decimal(20,0) NOT NULL,
  `CREATE_TIME` datetime NOT NULL,
  `UPDATE_TIME` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of bs_links
-- ----------------------------

-- ----------------------------
-- Table structure for bs_site
-- ----------------------------
DROP TABLE IF EXISTS `bs_site`;
CREATE TABLE `bs_site` (
  `ID` decimal(20,0) NOT NULL,
  `NAME` varchar(255) DEFAULT NULL COMMENT '站点名称',
  `EMAIL` varchar(255) DEFAULT NULL COMMENT '邮箱',
  `ICP` varchar(255) DEFAULT NULL COMMENT '工信ICP',
  `PICP` varchar(255) DEFAULT NULL COMMENT '公安ICP',
  `TEL` varchar(255) DEFAULT NULL COMMENT '电话',
  `CREATER` decimal(20,0) DEFAULT NULL,
  `CREATE_TIME` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `UPDATE_TIME` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of bs_site
-- ----------------------------
INSERT INTO `bs_site` VALUES ('1', '随心笔记', 'listcodes@gmail.com', '京ICP备14006229号-54', '京公网安备11011202001746号', '010-12345678', '1', '2019-12-08 22:27:11', null);

-- ----------------------------
-- Table structure for bs_user
-- ----------------------------
DROP TABLE IF EXISTS `bs_user`;
CREATE TABLE `bs_user` (
  `ID` decimal(20,0) NOT NULL,
  `DLM` varchar(20) NOT NULL COMMENT '登录名',
  `DLP` varchar(255) NOT NULL COMMENT '登录密码',
  `PHOTO` varchar(2000) DEFAULT NULL COMMENT '头像base64编码',
  `NICK` varchar(255) NOT NULL COMMENT '昵称',
  `STATE` decimal(1,0) DEFAULT NULL COMMENT '状态',
  `CREATER` decimal(20,0) NOT NULL COMMENT '创建人',
  `CREATE_TIME` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `UPDATE_TIME` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of bs_user
-- ----------------------------
INSERT INTO `bs_user` VALUES ('1', 'admin', '0', null, '管理员', '1', '1', '2019-12-08 21:53:44', null);


DROP TABLE IF EXISTS `bs_says`;
CREATE TABLE `bs_says` (
  `ID` decimal(20,0) NOT NULL,
  `MSG` varchar(500) DEFAULT NULL,
  `CREATER` decimal(20,0) DEFAULT NULL,
  `CREATE_TIME` datetime DEFAULT NULL,
  `UPDATE_TIME` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
